angular.module('tap.services', [])

.factory('Owner', function($window) {
  return {
    setDefaults: function() {
      var owner = {
        image: '',
        first_name: '',
        last_name: '',
        telephone: '',
        company: '',
        email: '',
        twitter: '',
        facebook: '',
        google_plus: '',
        linkedIn: ''
      };
      $window.localStorage.setItem('tcOwner', angular.toJson(owner));
      return owner;
    },
    toSimpleObject: function() {
      var details = this.get(), simple = {};
      Object.keys(details).forEach(function(key) {
        simple[key] = details[key];
      });
      return simple;
    },
    all: function() {
      return angular.fromJson($window.localStorage.getItem('tcOwner')) || this.setDefaults();
    },
    saveAll: function(newDetails) {
      var owner = this.all();
      Object.keys(owner).forEach(function(key) {
          owner[key] = newDetails[key] || '';
      });
      $window.localStorage.setItem('tcOwner', angular.toJson(owner));
      return owner;
    },
    saveField: function(key, val) {
      var owner = this.all();
      owner[key] = val;
      this.saveAll(owner);
    },
    clearField: function(key) {
      var owner = this.all();
      owner[key] = '';
      this.saveAll(owner);
    },
    get: function(field) {
      var owner = this.all();
      if (!field) return owner;
      return owner[field] || false;
    }
  }
})

.factory('Contacts', function($window, $cordovaContacts) {
  return {
    saveLastContact: function(data) {
      $window.localStorage.setItem('tcLastContact', angular.toJson(data));
    },
    getLastContact: function() {
      return angular.fromJson($window.localStorage.getItem('tcLastContact'));
    },
    saveContact: function(contact, success, fail) {
        // Create the contact
        var theNewContact = navigator.contacts.create();
        var name = new ContactName();
        name.givenName = contact.first_name;
        name.familyName = contact.last_name;
        theNewContact.name = name;

        theNewContact.displayName = contact.first_name || '';
        if (contact.last_name) {
          theNewContact.displayName = contact.first_name ? theNewContact.displayName + ' ' : '';
          theNewContact.displayName = theNewContact.displayName + contact.last_name;
        }

        if (contact.image) {
          //console.log('we have an image.');
          var photos = [];
          photos[0] = new ContactField('base64', contact.image, false);
          theNewContact.photos = photos;
        }

        if (contact.telephone) {
          var phoneNumbers = [];
          phoneNumbers[0] = new ContactField('main', contact.telephone, false);
          theNewContact.phoneNumbers = phoneNumbers;
        }
        
        if (contact.email) {
          var emails = [];
          emails[0] = new ContactField('main', contact.email, false);
          theNewContact.emails = emails;
        }

        theNewContact.save(success, fail);
    }
  }
});
