'use strict';

// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('tap', ['ionic', 'tap.controllers', 'tap.services', 'ngCordova'])

.run(function($ionicPlatform, $rootScope, $timeout, Owner, Contacts, $cordovaContacts) {

  $rootScope.tcReady = false, $rootScope.contacts = [];
  $ionicPlatform.ready(function() {

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    if (window.cordova && window.nfc) {
      //window.nfc.addMimeTypeListener('text/tapContact', function(nfcEvent){

      //console.log(':::addNdefListener.');
      window.nfc.addNdefListener(
        function(eve) {
          var json = angular.toJson(eve.tag);
          //console.log(json);
          //alert(json);

          var mimeType = 'text/taptap';
          var data = data || 'default data!';

          if (eve.tag.ndefMessage.length > 0) {
            eve.tag.ndefMessage.forEach(function(record, i) {
              //console.log(i,':', record);

              var recordType = nfc.bytesToString(record.type), payload;
              if (recordType === "T") {
                  var langCodeLength = record.payload[0],
                  text = record.payload.slice((1 + langCodeLength), record.payload.length);
                  payload = window.nfc.bytesToString(text);

              } else if (recordType === "U") {
                  var identifierCode = record.payload.shift(),
                  uri =  window.nfc.bytesToString(record.payload);

                  if (identifierCode !== 0) {
                      // TODO: Learn what this means...
                      //console.log("WARNING: uri needs to be decoded");
                  }
                  payload = uri;

              } else {
                  // kludge assume we can treat as String
                  payload = window.nfc.bytesToString(record.payload); 
              }
              //console.log('payload '+i,':', payload);
              var json = angular.fromJson(payload);
              //console.log('json', json);
              if (json) {
                //console.log('hmmmm...', json);
                $rootScope.$broadcast('contact:share');
                $rootScope.$broadcast('popup:show', json);
              }
          });
        }
      },
        function() {
          //console.log('Now listening!');
        },
        function(err) {
          //console.error('Unable to listen! '+err);
        }
      );
    }
    $rootScope.tcReady = true;
    $timeout(function() {
      $rootScope.$apply();
    })
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    // setup an abstract state for the tabs directive
    .state('tab', {
      url: "/tab",
      abstract: true,
      templateUrl: "templates/tabs.html"
    })

    // Each tab has its own nav history stack:

    .state('tab.dash', {
      url: '/dash',
      views: {
        'tab-dash': {
          templateUrl: 'templates/tab-dash.html',
          controller: 'DashCtrl'
        }
      }
    })

    .state('tab.new-contact', {
      url: '/new-contact',
      views: {
        'tab-friends': {
          templateUrl: 'templates/tab-new-contact.html',
          controller: 'NewContactCtrl'
        }
      }
    })
    .state('tab.friend-detail', {
      url: '/friend/:friendId',
      views: {
        'tab-friends': {
          templateUrl: 'templates/friend-detail.html',
          controller: 'FriendDetailCtrl'
        }
      }
    })

    .state('tab.account', {
      url: '/account',
      views: {
        'tab-account': {
          templateUrl: 'templates/tab-account.html',
          controller: 'AccountCtrl',
          resolve: {
            ownerdata: function(Owner, $q) {
              var deferred = $q.defer();
              deferred.resolve(Owner.toSimpleObject());
              return deferred.promise;
            }
          }
        }
      }
    })

    .state('tab.exit', {
      url:'/exit',
      views: {
        'tab-exit': {
          templateUrl: 'templates/tab-exit.html',
          controller: 'ExitCtrl'
        }
      }
    });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/dash');

});

