angular.module('tap.controllers', [])

.controller('DashCtrl', function($scope, $rootScope, Owner, $ionicPopup, $timeout, $state, Contacts) {
    $scope.owner = Owner.all();
    $scope.localstorage = window.localStorage.getItem('tcOwner');
    $scope.newContacts = [];
    $scope.tcReadyMessage = 'Starting...';

    $scope.goToAccount = function() {
      $state.go('tab.account');
    };

    $scope.share = function(method) {
      var owner = Owner.all(), message = [];
      if (window.ndef) {
        var mimeType = "text/taptap",
            payload = angular.toJson(owner),
            record = window.ndef.mimeMediaRecord(mimeType, window.nfc.stringToBytes(payload));
        message.push(record);
        //message.push(window.ndef.textRecord(angular.toJson(owner)));
        window.nfc.share(message,
          function(result) {
            alert('Details sent!', result);
          },
          function(err) {
            alert('Failed to send! ' + err);
          }
        );
      }
    };

    $scope.showPopup = function(contact) {
      //console.log('showPopup:contact:', contact);
      $scope.newContact = contact;
      var myPopup = $ionicPopup.show({
        template: '\
        <img ng-if="newContact.image" ng-src="{{newContact.image}}" />\
        <p ng-bind="newContact.first_name"></p>\
        <p ng-bind="newContact.last_name"></p>\
        <p ng-bind="newContact.telephone"></p>',
        title: 'New Contact',
        subTitle: 'Would you like to create a new contact?',
        scope: $scope,
        buttons: [
          { text: 'Cancel', type: 'button-default' },
          {
            text: '<b>Save</b>',
            type: 'button-positive',
            onTap: function(e) {
              if (!contact) {
                alert('No contact data received!');
                return true;
              } else {
                // Debugging
                $scope.newContacts.push(contact);
                Contacts.saveContact(contact, function(result) {
                  alert('Saved the contact! Awesome.');
                  //console.log(':::theResult:', result);
                }, function(err) {
                  alert('Didnt save the contact! Oh no!');
                  //console.error('Failed to save contact:', err);
                });
                /*$timeout(function() {
                  $scope.$apply();
                });*/
                return true;
              }
            }
          },
        ]
      });

      myPopup.then(function() {
        //console.log('Popped up!');
      });
    }

    var stopWatching = $rootScope.$watch('tcReady', function(v) {
      if (v) {
        $timeout(function() {
          //console.log(':::tcReady',v);
          //console.log(':::!!window.ndef:',!!window.ndef);
          $scope.tcReadyMessage = 'Ready!';
          $scope.share();
          /*$scope.$apply(function() {
          });*/
        });
      }
    });

    var endPopupListening = $scope.$on('popup:show', function(eve, contact) {
      //console.log('endPopupListening:eve:', eve);
      //console.log('endPopupListening:contact:', contact);
      $scope.showPopup(contact);
    });

    var stopSharing = $scope.$on('contact:share', function() {
      $scope.share();
    });

    $scope.$on('$destroy', function() {
      stopWatching();
      endPopupListening();
      stopSharing();
    });

    /*$timeout(function() {
      $scope.$apply(function() {
        console.log('started up!!!');
      });
    });*/
    //console.log('started up!!!');
  })

.controller('AccountCtrl', function($scope, $state, $rootScope, Owner, ownerdata, $ionicPopup, $cordovaCamera, $timeout) {
    //console.log('ownerdata', ownerdata);
    $scope.owner = ownerdata;
    $scope.save = function(newDetails) {
        try {
            var saved = Owner.saveAll(newDetails);
            alert('Contact details updated!');
        }
        catch (err) {
            alert('Unable to save contact details! '+err);
        }
    };

    $scope.clearField = function(field) {
      Contact.clearField(field);
      $scope.owner[field] = '';
    }

    $scope.choosePictureSource = function() {
      var myPopup = $ionicPopup.show({
        title: 'Get image from...',
        scope: $scope,
        buttons: [
          {
            text: 'Gallery',
            type: 'button-positive',
            onTap: function() {
              $scope.getPicture('gallery');
            }
          },
          {
            text: 'Camera',
            type: 'button-calm',
            onTap: function() {
              $scope.getPicture('camera');
            }
          },
        ]
      });
    }

    $scope.getPicture = function(source) {
      source = source == 'camera' ? navigator.camera.PictureSourceType.CAMERA : navigator.camera.PictureSourceType.PHOTOLIBRARY;

      var options = { 
        quality : 75, 
        destinationType : Camera.DestinationType.DATA_URL,
        sourceType : source,
        allowEdit : true,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 150,
        targetHeight: 150,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false
      };

      $cordovaCamera.getPicture(options).then(function(imageData) {
        // Success! Image data is here
        //console.log('got the image', 'data:image/jpeg;base64,'+imageData);
        $scope.owner.image = 'data:image/jpeg;base64,'+imageData;
        Owner.saveField('image', $scope.owner.image);
        $timeout(function() {
          $scope.$apply();
          //$state.go($state.current, {}, {reload: true});
        });
        //console.log($scope.owner);
      }, function(err) {
        // An error occured. Show a message to the user
        //console.log('failed to get the image:', err);
      });
    }
})

.controller('ExitCtrl', function() {
  ionic.Platform.exitApp();
});
